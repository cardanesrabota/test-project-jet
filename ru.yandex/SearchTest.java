package ru.yandex;

import org.junit.Test;
import org.openqa.selenium.By;


public class SearchTest extends Resources {



    //Показывает погоду - по запросу : "Погода"

    @Test
    public void weatherTest1() {

        weatherMethod("");

    }

    //Показывает погоду - по запросу : "Погода Липецк"

    @Test
    public void weatherTest2() {

        weatherMethod("Липецк");

    }

    //Парсит данные по первому suggest - по запросу : "Липецк"

    @Test
    public void testSearch1() {
        suggestMethod("Липецк");


    }

    //Парсит данные по первому suggest - по запросу : "Лото"

    @Test
    public void testSearch2() {
        suggestMethod("Лото");
    }


    //Открывает вкладку - ""Картинки" и проверяет ее URL и прогрузились ли элементы на страничке

    @Test
    public void testCheckImage() {

        driver.findElement(By.linkText("Картинки")).click();

        String url = driver.getCurrentUrl();

        if(url.equals("https://yandex.ru/images/")){
            System.out.println("URL тот же");
        } else {
            System.out.println("URL уже не тот");
        }

        existsElements("root");

    }

}

