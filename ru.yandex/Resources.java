package ru.yandex;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.NoSuchElementException;

public class Resources {

    WebDriver driver;
    WebDriverWait wait;

    @Before
    public void before() {
        System.setProperty("webdriver.chrome.driver", "/Users/daniilkraev/Downloads/chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 5);
        driver.manage().window().maximize();
        driver.get("https://yandex.ru/");


    }

    @After
    public void after() {
        driver.quit();

    }
    
    //Метод по выводу погоды в разных городах
    
    public void weatherMethod(String city) {

        driver.findElement(By.id("text")).sendKeys("Погода" + city);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("text")));

        List<WebElement> myList = driver.findElements(By.className("suggest2-item__fact"));

        System.out.println("Погода: "  + (myList.get(0).getAttribute("innerHTML").substring(51,57)));
    }
    
    //Метод по парсингу первого suggest из запроса

    public void suggestMethod(String search) {

        driver.findElement(By.id("text")).sendKeys(search);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("text")));

        WebElement myList = driver.findElement(By.className("suggest2-item__text"));

        System.out.println(myList.getAttribute("innerHTML"));
    }
    
    //Проверка на прогрузку элементов в разделе

    public boolean existsElements(String located) {
        try {
            driver.findElement(By.id(located));
        } catch (NoSuchElementException e){
            return false;
        }
        return true;
    }
}
