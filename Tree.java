
import java.util.Scanner;


public class Tree {

    static Scanner scan = new Scanner(System.in);
    static int height;

    public static void main(String[] args) throws Exception {

        System.out.print("How tall should the top of the tree be? ");
        height = scan.nextInt();
        System.out.println();

        //Тут задаем количество этажей елки:)



        if (height >= 6 && height <= 20) {
            System.out.println("Flat tree method:");
            flatTree();
            System.out.println("Xmas tree method:");
            xmasTree();
        } else {
            System.out.println("That's not a valid size. I can only do trees from 5 to 20");
            System.out.println("Quitting now.");
        }



    }

    public static void flatTree() {
        int width = (height * 2) - 1;

        for (int i = 1; i <= height; i++) {
            for (int stars = 1; stars <= width; stars++) {
                System.out.print(".");
            }
            System.out.println();
        }
// Тут нашу елочку центрируем
        for (int i = 0; i <= height / 5; i++) {
            if (height % 2 == 0) {
                for (int j = 0; j <= ((width) / 3) + 1; j++) {
                    System.out.print(".");

                }
            } else {

                for (int j = 0; j <= (width) / 3; j++) {
                    System.out.print(".");

                }
            }
            System.out.println();
        }

    }

    //Тут мы строим  крону нашей елочки

    public static void xmasTree() {
        int width = height * 2 - 1;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < height - i; j++) {
                System.out.print(" ");
            }

            for (int k = 0; k < (2 * i + 1 ); k++) {
                if(i == 0){
                    System.out.print("W");
                }else if (i > 0) {
                    if ((i % 2 != 0 && k == 0) || (i % 2 == 0 && k == (2 * i ))) {
                        System.out.print("@");
                    } else if ((i % 2 != 0 && k > 0)||(i % 2 == 0 && k < (2 * i + 1))) {
                        System.out.print("*");
                    }

                }
            }
            System.out.println();
        }
        for (int i = 0; i <= height / 5  ; i++) {
            for (int j = 0; j <= width / 3; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j <= (width) / 3; j++) {
                System.out.print("т");
            }
            System.out.println();
        }

    }

}

